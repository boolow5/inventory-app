import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import Cordova from './Cordova.js'
import i18n from 'vuex-i18n'
import vSelect from 'vue-select'
import VueRouter from 'vue-router'
import axios from 'axios'

import store from './store'
import router from './router'
import { sync } from 'vuex-router-sync'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import {dates, strings} from './mixins'
import Somali from './translations/so'

sync(store, router)

Vue.use(i18n.plugin, store)
Vue.i18n.add('so', Somali)
let lang = localStorage.getItem('lang')
if (!lang) {
  lang = 'en'
}
Vue.i18n.set(lang)

Vue.component('vue-select', vSelect)
Vue.mixin(dates)
Vue.mixin(strings)

Vue.use(Vuetify)

let DEBUG = true
window.baseURL = DEBUG ? 'http://localhost:8082' : 'http://api.qaado.com'

Vue.use(VueRouter)
var http = axios.create({
  baseURL: window.baseURL,
  timeout: 15 * 1000,
  headers: {
    'Content-Type': 'application/json'
  }
})

http.interceptors.request.use(config => {
  let auth = window.localStorage.getItem('auth')
  if (auth !== null) {
    auth = JSON.parse(auth)
    config.headers['Authorization'] = `Bearer ${(auth.jwt_token || auth.state.jwt_token)}`
  }
  return config
}, err => {
  console.log('axiso error', err)
})

http.interceptors.response.use(function (response) {
  if (response) {
    store.commit('NO_NETWORK', false)
  }
  return response
}, function (error) {
  return Promise.reject(error)
})

Vue.prototype.$http = http

export const EventBus = new Vue()

// Load Vue instance
export default new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
  mounted () {
    Cordova.initialize()
  },
  created () {
    window.VueInstance = this
  }
})
