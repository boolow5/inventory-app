import VueRouter from 'vue-router'

import {isLoggedIn} from '../store/modules/user'

import Home from '../components/Home.vue'
import About from '../components/About.vue'
import Shops from '../components/Shops.vue'
import Accounts from '../components/Accounts.vue'
import Performance from '../components/Performance.vue'
import Login from '../components/Login.vue'
import Users from '../components/Users.vue'

const router = new VueRouter({
  // mode: 'history',
  base: __dirname,
  routes: [
    { name: 'Home', path: '*', component: Home, meta: {title: 'Home', authRequired: true} },
    { name: 'About', path: '/about', component: About, meta: {title: 'About', authRequired: false} },
    { name: 'Shops', path: '/shops', component: Shops, meta: {title: 'Shops', authRequired: true} },
    { name: 'Accounts', path: '/accounts', component: Accounts, meta: {title: 'Accounts', authRequired: true} },
    { name: 'Performance', path: '/performance', component: Performance, meta: {title: 'Performance', authRequired: true} },
    { name: 'Login', path: '/login', component: Login, meta: {title: 'Login', noLayout: true, authRequired: false} },
    { name: 'Users', path: '/users', component: Users, meta: {title: 'Users', authRequired: true} }
  ]
})

router.beforeEach((to, from, next) => {
  // console.log('beforeEach', {to, from, next})
  to.matched.some(route => {
    document.title = route.meta.title + ' | Qaado Inventory'
    if (route.meta.authRequired && !isLoggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  })
})

export default router
