const trimspaces = function (str) {
  if (typeof str === 'string') {
    return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '')
  }
  return str
}

const trim = function (str, length) {
  if (typeof str === 'string') {
    return str.substring(0, length) + (str.length > length ? '...' : '')
  }
  return str
}

const trimmiddle = function (str, length, length2) {
  if (typeof str === 'string') {
    if (str.length <= length) {
      return str
    }
    if (!length2) {
      length2 = 5
    }
    if (str.length <= length - length2) {
      return str.substring(0, length) + (str.length > length ? '...' : '')
    }
    let part1 = str.substring(0, length - length2)
    let part2 = str.substring(str.length - length2, str.length)
    return part1 + '....' + part2
  }
  return str
}

const toaddress = function (obj) {
  if (!obj) {
    return ''
  }
  if (typeof obj === 'object') {
    let txt = ''
    if (obj.name) {
      if (obj.name.indexOf(obj.city) > -1 || obj.name.indexOf(obj.country) > -1) {
        return obj.name
      }
      txt += obj.name
    }
    if (obj.city) {
      txt += txt !== '' ? ', ' : ''
      txt += obj.city
    }
    if (obj.country) {
      txt += txt !== '' ? ', ' : ''
      txt += obj.country
    }
    return txt
  }
  return obj
}

const datasize = function (sizeStr) {
  // console.log('datasize', sizeStr)
  var s
  try {
    s = parseFloat(sizeStr)
    if (s < 1024) {
      return (s).toFixed(1) + 'B'
    }
    if (s < 1024000) {
      return (s / 1024).toFixed(1) + 'KB'
    }
    if (s < 1024000000) {
      return (s / 1000000).toFixed(1) + 'MB'
    }
    if (s < 1024000000000) {
      return (s / 1000000000).toFixed(1) + 'GB'
    }
    if (typeof s === 'number') {
      return s.toFixed(1)
    }
    return s
  } catch (error) {
    // console.log('datasize error', error)
    return sizeStr
  }
}

export default {
  filters: {
    trimspaces,
    trim,
    toaddress,
    datasize,
    trimmiddle
  }
}
