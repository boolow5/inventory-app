const toLocaleString = function (arg) {
  if (typeof arg === 'string' || typeof date === 'number') {
    return new Date(arg).toLocaleString('en-GB')
  }
  if (typeof arg === 'object' && arg.consturctor.name === 'Date') {
    return arg.toLocaleString('en-GB')
  }
  return arg
}

const toLocaleDateString = function (date) {
  if (typeof date === 'string' || typeof date === 'number') {
    return new Date(date).toLocaleDateString('en-GB')
  }
  if (typeof date === 'object' && date.consturctor.name === 'Date') {
    return date.toLocaleDateString('en-GB')
  }
  return date
}

const toDateDiffString = function (date) {
  let diff = 0
  let now = new Date()
  if (typeof date === 'string' || typeof date === 'number') {
    date = new Date(date)
    diff = now.getTime() - date.getTime()
  } else if (typeof date === 'object' && date.consturctor.name === 'Date') {
    diff = now.getTime() - date.getTime()
  } else {
    return date
  }

  let isBefore = diff > 0
  // is this changed yet
  // diff in minutes
  diff = Math.abs(Math.ceil(diff / (1000 * 60)))
  // more than year
  if (diff > 365 * 24 * 60) {
    diff = Math.ceil(diff / (365 * 60))
    return (isBefore ? `${diff}y` : `after ${diff}y`)
  } else if (diff > 30 * 24 * 60) { // more than a month
    diff = Math.ceil(diff / (30 * 24 * 60))
    return (isBefore ? `${diff}m` : `after ${diff}M`)
  } else if (diff > 24 * 60) { // more than a day
    diff = Math.ceil(diff / (24 * 60))
    return (isBefore ? `${diff}d` : `after ${diff}d`)
  } else if (diff > 1 * 60) {
    diff = Math.ceil(diff / 60)
    return (isBefore ? `${diff}h` : `after ${diff}h`)
  } else if (diff > 0) {
    // console.log('before ' + diff, Math.ceil(diff))
    diff = Math.ceil(diff)
    return (isBefore ? `${diff}m` : `after ${Math.ceil(diff)}m`)
  }
  return ''
}

const shortNumber = function (num) {
  let result = num
  if (typeof num !== 'number') {
    result = Number(num)
    if (isNaN(result)) {
      return num
    }
  }
  if (result < 1000) {
    return result
  }
  if (result < 1000000) {
    return `${(result / 1000).toFixed(1)}k`
  }
  if (result < 1000000000) {
    return `${(result / 1000000).toFixed(2)}m`
  }
  if (result < 1000000000000) {
    return `${(result / 1000000000).toFixed(3)}b`
  }
  if (result < 1000000000000000) {
    return `${(result / 1000000000000).toFixed(3)}t`
  }
  return result
}

export default {
  filters: {
    toLocaleString,
    toLocaleDateString,
    toDateDiffString,
    shortNumber
  }
}
