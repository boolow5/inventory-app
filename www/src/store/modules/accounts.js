const state = {
  accounts: [],
  selected_account: null,
  available_currencies: [
    {name: 'US Dollar', code: 'USD'},
    {name: 'Somali Shilling', code: 'SOS'},
    {name: 'Kenyan Shilling', code: 'KES'}
  ]
}

const getters = {
  accounts: state => state.accounts,
  selected_account: state => {
    if (state.selected_account === null) {
      let selectedAccount = localStorage.getItem('selected_account')
      return JSON.parse(selectedAccount) || null
    }
    return state.selected_account
  },
  available_currencies: state => state.available_currencies,
  get_currency_name: state => code => {
    if (!code) {
      return ''
    }
    const [currency] = state.available_currencies.filter(item => item.code === code)
    if (currency && currency.name) {
      return currency.name
    }
    return ''
  }
}

const mutations = {
  SELECT_ACCOUNT (state, account) {
    console.log('SELECT_ACCOUNT', account)
    state.selected_account = account
  },
  ADD_ACCOUNT (state, account) {
    if (account && account.id) {
      state.accounts.push(account)
    }
  },
  SET_ACCOUNTS (state, accounts) {
    state.accounts = accounts
  },
  REMOVE_ACCOUNT (state, accountID) {
    if (accountID) {
      state.accounts = state.accounts.filter(item => item.id !== accountID)
    }
  }
}

const actions = {
}

export default {
  state,
  getters,
  mutations,
  actions
}
