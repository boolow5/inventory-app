const state = {
  shops: [],
  selected_shop: null
}

const getters = {
  shops: state => state.shops,
  selected_shop: state => {
    if (state.selected_shop === null) {
      let selectedShop = localStorage.getItem('selected_shop')
      return JSON.parse(selectedShop) || null
    }
    return state.selected_shop
  }
}

const mutations = {
  SELECT_SHOP (state, shopID) {
    for (let i = 0; i < state.shops.length; i++) {
      if (state.shops[i].id === shopID) {
        state.selected_shop = state.shops[i]
        localStorage.setItem('selected_shop', JSON.stringify(state.selected_shop))
        return
      }
    }
  },
  ADD_SHOP (state, shop) {
    if (shop && shop.id) {
      state.shops.push(shop)
    }
  },
  SET_SHOPS (state, shops) {
    state.shops = shops
  },
  REMOVE_SHOP (state, shopID) {
    if (shopID) {
      state.shops = state.shops.filter(item => item.id !== shopID)
    }
  }
}

const actions = {
}

export default {
  state,
  getters,
  mutations,
  actions
}
