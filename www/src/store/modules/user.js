export const isLoggedIn = function () {
  // console.log('isLoggedIn', {length: state.jwt_token.length, str: state.jwt_token, isTrue: state && state.jwt_token && state.jwt_token.length > 50})
  return state && state.jwt_token && state.jwt_token.length > 50
}

const state = {
  jwt_token: '',
  level: null,
  profile: {},
  role: null,
  users: []
}

const getters = {
  is_logged_in: (state) => state.jwt_token && state.jwt_token.length > 50,
  profile: (state) => state.profile,
  users: (state) => state.users
}

const mutations = {
  SET_USERS (state, users) {
    state.users = users
  },
  SET_USER_DATA (state, data) {
    console.log('SET_USER_DATA', [(data && (data.token || data.jwt_token)), data])
    if (data && (data.token || data.jwt_token)) {
      state.level = data.level
      state.jwt_token = data.jwt_token || data.token
      localStorage.setItem('auth', JSON.stringify({jwt_token: state.jwt_token, level: state.level}))
    }
  },
  SET_TOKEN (state, token) {
    // console.log('SET_TOKEN', token)
    state.jwt_token = token
    localStorage.setItem('auth', JSON.stringify({jwt_token: state.jwt_token, level: state.level}))
  },
  SET_LEVEL (state, level) {
    state.level = level
  },
  SET_USER_LEVEL (state, level) {
    state.role = level
  },
  SET_PROFILE (state, profile) {
    state.profile = profile
  },
  CLEAR_USER_DATA (state) {
    state.jwt_token = ''
    state.level = null
    state.profile = {}
  }
}

const actions = {
}

export default {
  state,
  getters,
  mutations,
  actions
}
