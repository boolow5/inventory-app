const state = {
  drawer: true,
  layout: true,
  loading: {enable: false, msg: 'Initializing...'},
  listModeHome: true,
  defaultImage: 'not-found.png',
  filter_sidebar: false,
  notifications: {
    enable: true,
    count: function () {
      return this.items ? this.items.length : 0
    },
    items: []
  },
  popupOpen: false,
  noNetwork: false
}

const getters = {
  drawer: (state) => state.drawer,
  layout: (state) => state.layout,
  app_loading: (state) => state.loading,
  listModeHome: (state) => state.listModeHome,
  defaultImage: (state) => state.defaultImage,
  filter_sidebar: (state) => state.filter_sidebar,
  notifications: (state) => state.notifications,
  popupOpen: (state) => state.popupOpen,
  noNetwork: (state) => state.noNetwork,
  hasNetwork: (state) => !state.noNetwork
}

const mutations = {
  DRAWER (state, value) {
    state.drawer = value === true
  },
  APP_LOADING (state, value) {
    // console.log('APP_LOADING', value)
    if (typeof value === 'boolean') {
      state.loading.enable = value
    } else if (typeof value === 'string') {
      state.loading.msg = value
    } else if (typeof value === 'object' && value.hasOwnProperty('enable')) {
      state.loading = value
    }
  },
  IS_LIST_MODE_HOME (state, value) {
    state.listModeHome = value === true
  },
  SET_FILTER_SIDEBAR (state, value) {
    state.filter_sidebar = value === true
  },
  TOGGLE_NOTIFICATION (state, payload) {
    state.notifications.enable = payload === true
  },
  INSERT_NOTIFICATION_ITEM (state, payload) {
    if (typeof payload === 'string') {
      state.notifications.items.push(JSON.parse(payload))
    } else if (typeof payload === 'object') {
      state.notifications.items.push(payload)
    }
  },
  SET_NOTIFICATION_ITEM (state, payload) {
    state.notifications.items = payload
  },
  SET_POPUP (state, payload) {
    state.popupOpen = payload === true
  },
  NO_NETWORK (state, payload) {
    state.noNetwork = payload === true
  }
}

const actions = {
  CLEAR_ALL (context) {
    context.commit('CLEAR_USER_DATA')
  },
  HANDLE_ERROR ({commit}, err) {
    console.log('HANDLE_ERROR', JSON.parse(JSON.stringify(err)))
    if (err.toString().indexOf('Network Error') > -1) {
      commit('NO_NETWORK', true)
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
