import Vue from 'vue'
import Vuex from 'vuex'

import app from './modules/app'
import user from './modules/user'
import shops from './modules/shops'
import accounts from './modules/accounts'

Vue.use(Vuex)

// const LocalStorage = (store) => {
//   store.subscribe((mutation) => {
//     window.localStorage.setItem('auth', JSON.stringify(user.state))
//     if (mutation.type === 'CLEAR_ALL') {
//       window.localStorage.removeItem('auth')
//     }
//   })
// }

export default new Vuex.Store({
  modules: {
    app,
    user,
    accounts,
    shops
  }
  // plugins: [LocalStorage]
})
