#!/bin/bash

export HOST="news-api.iweydi.com"

# Package name
export APP_PACKAGE=qaado-inventory-app.tar.gz

# The remote directory of the server
export DEPLOY_PATH=/var/www
# static files directory
export STATIC_PATH=/webapps/news-crawler/www/app
# source directory
export SOURCE_DIR=./
# remote server user
export OWNER=ubuntu
# # web user that runs the web app
# export WEB_USER=news-crawler
# # process name
# export PROC_NAME=qaado-inventory-app
echo "removing files from ./dist/*"
rm -r ./dist/*
echo "copying browser files to ./dist"
cp -r ./platforms/browser/www/* ./dist
echo "updating static files in ./dist/dist"
cp -r ./www/dist/* ./dist/dist

echo "Packaging files in ./dist"
tar -zcf $APP_PACKAGE ./dist/*

scp -i "$HOME/mahdi-ec2-1.pem" $APP_PACKAGE ${OWNER}@${HOST}:/tmp

echo "Removing sent files";
rm -rf $APP_PACKAGE;

# echo "sending \"$APP_PACKAGE\" to \"$DEPLOY_PATH/bin\""

ssh -i "$HOME/mahdi-ec2-1.pem" -t ${OWNER}@${HOST} "
  sudo tar -xvzf /tmp/$APP_PACKAGE -C $DEPLOY_PATH --strip-components=1;
  sudo rm -r $DEPLOY_PATH/news-app;
  sudo mv $DEPLOY_PATH/dist $DEPLOY_PATH/news-app;
  sudo service nginx restart || true
";
