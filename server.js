const express = require('express')
const path = require('path')
const serveStatic = require('serve-static')

const app = express()

app.use('/', serveStatic(path.join(__dirname,
  process.env.DEV ? '/platforms/browser/www/dist' : '/www/dist'
)))

app.get('*', function (req, res) {
  let filePath = path.join(__dirname, '/www/index.html')
  if (process.env.DEV) {
    filePath = path.join(__dirname, '/platforms/browser/www/index.html')
  }
  console.log('[express] ' + new Date() + '\t' + req.url + '\t', filePath)
  res.sendFile(filePath)
})

const port = process.env.PORT || 5000
app.listen(port, () => {
  console.log('Listening on port ' + port)
})
